#include <iostream>
#include <funcoes.h>
using namespace std;

int main()
{
    //DEFININDO OS REGISTRADORES DO PROCESSADOR INICIALMENTE COMO 0
    registrador PC=0,IR=0,MBR=0,MAR=0,MQ=0,AC=0;
   //DEFININDO A ESTRUTURA DE MEMORIA
    memoria M;
    //INICIANDO A MEMORIA COM ZEROS
    zeramemoria(M);
    instrucoes(M);
    //menu(M);
    //mostrarmemoria(M);
    processador(M,PC,IR,MAR,MBR,AC,MQ);
    cout <<endl<<"M[606] = "<<M.linha[606]<<endl;
}
