#ifndef FUNCOES_H
#define FUNCOES_H
#include "estruturas.h"
#include <cmath>
#include <iostream>
using namespace std;

//FUNCAO QUE RETORNA O PRIMEIRO DIGITO
int opcode(int a){
    int numero;
    numero = a;
    while(numero>10){
        numero = numero/10;
    }
    return numero;
}

//FUNCAO QUE RETORNA OS N-1 DIGITOS
int endereco(int a){
    int numero,resto,soma=0,cont=0;
    numero = a;
    while(numero>10){
        resto = numero%10;
        numero = numero/10;
        soma = soma+resto*pow(10,cont);
        cont++;
    }
    return soma;
}

//FUNCAO QUE CONTA A QUANTIDADE DE CASAS DECIMAIS E RETORNA EM BASE 10
int casas_decimais(int a){
    int numero,cont=0;
    numero = a;
    while(numero!=0){
        numero = numero/10;
        cont++;
    }
    return pow(10,cont);
}

//FUNCAO QUE ATRIBUI ZERO A TODOS OS CAMPOS DA MEMORIA PRINCIPAL
void zeramemoria(memoria &M){
    for(int i=0;i<1024;i++){
        M.linha[i]=0;
    }
}

//FUNCAO QUE IMPRIMI A MEMORIA
void mostrarmemoria(memoria &M){
    for(int i=0;i<1024;i++){
        cout <<M.linha[i]<<endl;
    }
}

void busca(memoria &M,registrador &PC,registrador &IR,registrador &MAR,registrador &MBR){
    //MICRO OPERACAO DE COPIA DO PC PARA O MAR
    MAR = PC;
    //MICRO OPERAÇÕES DE LEITURA DO CONTEUDO NA MEMORIA APONTADA PELO MAR E GRAVAÇÃO DESSE CONTEUDO NO MBR
    MBR = M.linha[MAR];
    //MICRO OPERACAO DE COPIA DO CONTEUDO DO MBR NO IR
    IR = MBR;
    //MIRCRO OPERAÇÕES LEITURA E ESCRITA DO PC (IMCREMENTAÇÃO DO PC)
    PC++;
}
void execucao(memoria &M,registrador &IR,registrador &MAR,registrador &MBR,registrador &AC,registrador &MQ){
    int op;
    //DECODIFICACAO DA INTRUCAO E DO ENDERECO
    op = opcode(IR);
    MAR = endereco(IR);

    //DECODIFICACAO E EXECUCAO
    switch (op) {
    //LOAD MQ
    case 1:
        //MICRO OPERACAO DE TRASFERIR O VALOR DE MQ PARA AC
        MBR = MQ;
        AC = MBR;
        cout <<"LOAD MQ"<<endl;
        break;
        //LOAD MQ, X
    case 2:
        //MICRO OPERACAO DE TRANSFERENCIA DO CONTEUDO X PARA MQ
        MBR = M.linha[MAR];
        MQ = MBR;
        cout <<"LOAD MQ, "<<MAR<<endl;
        break;
        //STOR X
    case 3:
        //MIRCRO OPERACOES QUE GRAVAM O VALOR DE AC NO ENDERECO X DE MEMORIA
        MBR = AC;
        M.linha[MAR] = MBR;
        cout <<"STOR "<<MAR<<endl;
        break;
        //LOAD X
    case 4:
        //MIRCRO OPERACOES QUE GRAVAM O VALOR ARMAZENADO NO ENDERECO X DE MEMORIA EM AC
        MBR = M.linha[MAR];
        AC = MBR;
        cout <<"LOAD "<<MAR<<endl;
        break;
        //ADD X
    case 5:
        //MIRCRO OPERACOES QUE SOMAM O VALOR ARMAZENADO NO ENDERECO X DE MEMORIA COM AC
        MBR = M.linha[MAR];
        AC = AC + MBR;
        cout <<"ADD "<<MAR<<endl;
        break;
        //SUB X
    case 6:
        //MIRCRO OPERACOES QUE SUBTRAEM O VALOR ARMAZENADO NO ENDERECO X DE MEMORIA COM AC
        MBR = M.linha[MAR];
        AC = AC - MBR;
        cout <<"SUB "<<MAR<<endl;
        break;
        //MUL X
    case 7:
        //MIRCRO OPERACOES QUE MULTIPLICA O VALOR ARMAZENADO NO ENDERECO X DE MEMORIA COM MQ
        MBR = M.linha[MAR];
        MQ = MBR * MQ;
        cout <<"MUL "<<MAR<<endl;
        break;
        //DIV X
    case 8:
        //MIRCRO OPERACOES QUE DIVIDE  AC COM O VALOR ARMAZENADO NO ENDERECO X DE MEMORIA E  ARMAZENA O COCIENTE EM MQ E O RESTO EM AC
        MBR = M.linha[MAR];
        MQ = AC/MBR;
        AC = AC%MBR;
        cout <<"DIV "<<MAR<<endl;
        break;
    default:
        break;
    }
}

//FUNCAO QUE SIMULA A EXECUCAO DE UM PROCESSADOR
void processador(memoria &M,registrador &PC,registrador &IR,registrador &MAR,registrador &MBR,registrador &AC,registrador &MQ){
    while(PC <=511){
        busca(M,PC,IR,MAR,MBR);
        execucao(M,IR,MAR,MBR,AC,MQ);
    }
}

//FUNCAO QUE REALIZA QUE IMPRIMI UM ESPACAMENTO
void space(int n){
    for(int i=0;i<n;i++)
        cout <<endl;
}

//FUNCAO QUE PERMITE QUE O USUARIO INSIRA NA MEMORIA VALORES INICIAIS E INSTRUCOES
void menu(memoria &principal){
    int op,op2,end,valor,cont=0;
    cout << "SIMULADOR DE UM PROCESSADOR"<<endl;
    do{
        cout <<"MENU:"<<endl;
        cout <<"1-ADICIONAR UM VALOR A UM ENDERECO DE MEMORIA( 512 A 1023)."<<endl;
        cout <<"2-ADICIONAR UMA INSTRUCAO NA MEMORIA."<<endl;
        cout <<"-1 - SAIR."<<endl;
        cout <<"DIGITE A SUA OPCAO:";
        cin >> op;
        while(op<-1 || op>2){
            cout <<"OPCAO INVALIDA!!! DIGITE NOVAMENTE:";
            cin >> op;
        }
        if(op == 1){
            cout <<"DIGITE O ENDERECO DE MEMORIA A SER ATRIBUIDO O VALOR:";
            cin >>end;
            while(end <512 || end >1023){
                cout <<"ENDERECO NAO PERMITIDO!!! DIGITE UM VALOR ENTRE 512 E 1023:";
                cin >> end;
            }
            cout <<"DIGITE O VALOR A SER ARMAZENADO NO ENDERECO DE MEMORIA "<<end<<" :";
            cin >>valor;
            while(valor <-32677 || valor >32677){
                cout <<"VALOR NAO PERMITIDO!!! DIGITE UM VALOR ENTRE -32677 E 32677:";
                cin >> valor;
            }
            principal.linha[end]=valor;
        }else if(op==2){
            space(5);
            cout << "ESCOLHA O TIPO DE COMANDO QUE VC QUER ENSERIR:"<<endl;
            cout <<"1-LOAD MQ"<<endl;
            cout <<"2-LOAD MQ, X"<<endl;
            cout <<"3-STOR X"<<endl;
            cout <<"4-LOAD X"<<endl;
            cout <<"5-ADD X"<<endl;
            cout <<"6-SUB X"<<endl;
            cout <<"7-MUL X"<<endl;
            cout <<"8-DIV X"<<endl;
            cout <<"0 - SAIR."<<endl;
            cout <<"DIGITE A SUA OPCAO:";
            cin >> op2;
            while(op2<0 || op2>8){
                cout <<"OPCAO INVALIDA!!! DIGITE NOVAMENTE:";
                cin >> op2;
            }
            if(op2 == 1){
                principal.linha[cont]=1999;
                cont++;
            }
            if(op2 == 2){
                cout <<"DIGITE O ENDERECO DE MEMORIA A TRNASFERIR(X):";
                cin >>end;
                while(end <512 || end >1023){
                    cout <<"ENDERECO NAO PERMITIDO!!! DIGITE UM VALOR ENTRE 512 E 1023:";
                    cin >> end;
                }
                principal.linha[cont]=2*casas_decimais(end)+end;
                cont++;
            }
            if(op2 == 3){
                cout <<"DIGITE O ENDERECO DE MEMORIA (X) PARA RECEBER AC:";
                cin >>end;
                while(end <512 || end >1023){
                    cout <<"ENDERECO NAO PERMITIDO!!! DIGITE UM VALOR ENTRE 512 E 1023:";
                    cin >> end;
                }
                principal.linha[cont]=3*casas_decimais(end)+end;
                cont++;
            }
            if(op2 == 4){
                cout <<"DIGITE O ENDERECO DE MEMORIA A TRNASFERIR(X) PARA AC:";
                cin >>end;
                while(end <512 || end >1023){
                    cout <<"ENDERECO NAO PERMITIDO!!! DIGITE UM VALOR ENTRE 512 E 1023:";
                    cin >> end;
                }
                principal.linha[cont]=4*casas_decimais(end)+end;
                cont++;
            }
            if(op2 == 5){
                cout <<"DIGITE O ENDERECO DE MEMORIA (X) PARA SOMAR COM AC:";
                cin >>end;
                while(end <512 || end >1023){
                    cout <<"ENDERECO NAO PERMITIDO!!! DIGITE UM VALOR ENTRE 512 E 1023:";
                    cin >> end;
                }
                principal.linha[cont]=5*casas_decimais(end)+end;
                cont++;
            }
            if(op2 == 6){
                cout <<"DIGITE O ENDERECO DE MEMORIA (X) PARA SUBTRAIR COM AC:";
                cin >>end;
                while(end <512 || end >1023){
                    cout <<"ENDERECO NAO PERMITIDO!!! DIGITE UM VALOR ENTRE 512 E 1023:";
                    cin >> end;
                }
                principal.linha[cont]=6*casas_decimais(end)+end;
                cont++;
            }
            if(op2 == 7){
                cout <<"DIGITE O ENDERECO DE MEMORIA (X) PARA MULTIPLICAR COM MQ:";
                cin >>end;
                while(end <512 || end >1023){
                    cout <<"ENDERECO NAO PERMITIDO!!! DIGITE UM VALOR ENTRE 512 E 1023:";
                    cin >> end;
                }
                principal.linha[cont]=7*casas_decimais(end)+end;
                cont++;
            }
            if(op2 == 8){
                cout <<"DIGITE O ENDERECO DE MEMORIA (X) A SER DIVIDIDO COM AC:";
                cin >>end;
                while(end <512 || end >1023){
                    cout <<"ENDERECO NAO PERMITIDO!!! DIGITE UM VALOR ENTRE 512 E 1023:";
                    cin >> end;
                }
                principal.linha[cont]=8*casas_decimais(end)+end;
                cont++;
            }
        }
    }while(op!=-1);
}

//FUNCAO COM VALORES PRE DETERMINADOS DE VALORES A SEREM ARMAZENADOS NA MEMORIA E INSTRUCOES QUE CALCULEM A SEGUINTE EQUACAO X = (A-B*C)/(D-E*F)
void instrucoes(memoria &M){
    M.linha[0] = 2602;
    M.linha[1] = 7601;
    M.linha[2] = 1999;
    M.linha[3] = 5600;
    M.linha[4] = 3607;
    M.linha[5] = 2604;
    M.linha[6] = 7605;
    M.linha[7] = 1999;
    M.linha[8] = 3608;
    M.linha[9] = 4603;
    M.linha[10] = 6608;
    M.linha[11] = 3609;
    M.linha[12] = 4603;
    M.linha[13] = 6608;
    M.linha[14] = 3609;
    M.linha[15] = 4607;
    M.linha[16] = 8609;
    M.linha[17] = 3607;
    M.linha[18] = 1999;
    M.linha[19] = 3606;

    //DADOS
    M.linha[600] = 37; //A
    M.linha[601] = 80; //B
    M.linha[602] = 23; //C
    M.linha[603] = 16; //D
    M.linha[604] = 63; //E
    M.linha[605] = 11; //F
    M.linha[606] = 0; //X

}

#endif // FUNCOES_H
