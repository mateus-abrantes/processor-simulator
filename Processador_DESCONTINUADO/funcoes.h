#ifndef FUNCOES_H
#define FUNCOES_H
#include "estruturas.h"
#include <string>
#include <algorithm>
#include <iostream>
#include <cmath>
using namespace std;


//FUNCAO QUE CONVERTE UM NUMERO INTEIRO EM UM VETOR BINARIO DE 16 BITS
void binario(registrador &V,int numero){
    int aux =  numero,resto,i=15;
    do{
        resto = aux%2;
        V.linha[i] = resto;
        aux = aux/2;
        i--;
    }while(aux !=0);
    for(int a=0;a<=i;a++){
        V.linha[a] = 0;
    }
}
//FUNCAO QUE CONVERTE UM NUMERO INTEIRO EM BINARIO PARA ESTRUTURA WORD
void binario(word &V,int numero){
    int aux =  numero,resto,i=15;
    do{
        resto = aux%2;
        V.linha[i] = resto;
        aux = aux/2;
        i--;
    }while(aux !=0);
    for(int a=0;a<=i;a++){
        V.linha[a] = 0;
    }
}



//FUNCAO QUE COMPARA OS PRIMEIROS 6 BITS DE UM VETOR DE 16 BITS COM UMA STRING DE 6 BITS
bool compare(registrador A,string B){
    bool com = true;
    string aux;
    for(int i=0;i<6;i++){
        aux = B[i];
        if(A.linha[i]==stoi(aux)){
            com = true;
        }else{
            return false;
        }
    }
    return com;
}

//FUNCAO QUE COPIA A INSTRUCAO DE 16 BITS PARA UM REGISTRADOR DE 16 BITS
void copia_word_para_registrador(word A,registrador &B){
    int i;
    for(i = 0;i<16;i++){
        B.linha[i] = A.linha[i];
    }
}

//FUNCAO QUE COPIA UM REGISTRADOR EM OUTRO
void copia_registrador1_para_registrador2(registrador A,registrador &B){
    int i;
    for(i = 0;i<16;i++){
        B.linha[i] = A.linha[i];
    }
}

//FUNCAO QUE COPIA UM REGISTRADOR EM OUTRO
void copia_registrador_para_word(word &A,registrador B){
    for(int i = 0;i<16;i++){
        A.linha[i] = B.linha[i];
    }
}

//FUNCAO QUE COPIA OS ULTIMOS 10 BITS DE UM VETOR DE 16 BITS PARA UM VETOR DE 16 BITS
void copia_os_10b_do_registador1_para_registrador2(registrador A,registrador &B){
    int aux=0;
    int i;
    for(i = 6;i<16;i++){
        B.linha[i] = A.linha[i];
        aux++;
    }
    for(int a=0;a<6;a++){
        B.linha[a] = 0;
    }
}

//FUNCAO QUE COPIA OS PRIMEIROS 6 BITS DE UM VETOR DE 16 BITS PARA UM VETOR DE 16 BITS
void copia_os_4b_do_word_para_registrador(word A,registrador &B){
    for(int i = 0;i<16;i++){
        if(i<6){
            B.linha[i] = A.linha[i];
        }else{
            B.linha[i]=0;
        }
    }
}

//FUNCAO QUE COMVERTE OS 16  BITS ARMAZENADOS EM UM VETOR EM UM NUMERO DECIMAL INTEIRO
int decimal_word(word &V){
    int tam = 16,num=0,a=0;
    for(int i=tam-1; i>=0;i--){
        num = num + V.linha[i]*pow(2,a);
        a++;
    }
    return num;
}
//FUNCAO QUE RECEBE OS 10 ULTIMOS BITS DE UM VETOR E TRANSFORMA EM DECIMAL
int decimal_registrador_10B(registrador &V){
    int tam = 16,num=0,a=0;
    for(int i=tam-1; i>10;i--){
        num = num + V.linha[i]*pow(2,a);
        a++;
    }
    return num;
}
//FUNCAO QUE RECEBE VETOR DE 16 BITS E TRANSFORMA EM DECIMAL
int decimal_registrador_16B(registrador &V){
    int tam = 16,num=0,a=0;
    for(int i=tam-1; i>=0;i--){
        num = num + V.linha[i]*pow(2,a);
        a++;
    }
    return num;
}

void mostrarlinhamemoria(memoria a,int linha){
    for(int i=0;i<16;i++){
        cout <<a.mem[linha].linha[i]<< " ";
    }
    cout <<endl;
}

void mostrarword(word a){
    for(int i=0;i<16;i++){
        cout <<a.linha[i]<< " ";
    }
    cout <<endl;
}

void mostrarregistrador(registrador A){
    for(int i=0;i<16;i++){
        cout <<A.linha[i]<< " ";
    }
    cout <<endl;
}

void soma(registrador &A, registrador B){
    int auxA, auxB;
    auxA = decimal_registrador_16B(A);
    auxB = decimal_registrador_16B(B);
    binario(A,auxA+auxB);
}

void subtracao(registrador &A, registrador B){
    int auxA, auxB;
    auxA = decimal_registrador_16B(A);
    auxB = decimal_registrador_16B(B);
    binario(A,auxA-auxB);
}

void multiplicacao(registrador &A, registrador B, registrador &MQ){
    int auxMQ, auxB;
    auxMQ = decimal_registrador_16B(MQ);
    auxB = decimal_registrador_16B(B);
    binario(A,auxMQ*auxB);
}

void divisao(registrador &AC, registrador MBR, registrador &MQ){
    int auxMBR,auxAC;
    auxAC = decimal_registrador_16B(AC);
    auxMBR = decimal_registrador_16B(MBR);
    binario(MQ,((auxAC)/(auxMBR)));
    binario(AC,((auxAC)%(auxMBR)));
}

string bin(int numero){
    int aux =  numero,resto,i=0;
    string resultado;
    do{
        resto = aux%2;
        resultado+= to_string(resto);
        aux = aux/2;
        i++;
    }while(aux !=0);
    reverse(resultado.begin(), resultado.end());
    return resultado;
}

void gravar(word &linha,string comando){
    string aux;
    for(int i=0;i<16;i++){
        aux = comando[i];
        linha.linha[i]=stoi(aux);
    }
}


void execucao(memoria &principal,registrador &PC,registrador &IR,registrador &MAR,registrador &MBR,registrador &AC,registrador &MQ){

    //DECODIFICACAO DA INSTRUCAO
    //OPERACAO LOAD MQ
    //TRANSFERE O CONTEUDO DO MQ PARA O AC
    if(compare(IR,"001010")){
        copia_registrador1_para_registrador2(MQ,AC);
        cout<<"LOAD MQ"<<endl;
        cout<<"MQ:";
        mostrarregistrador(MQ);
        cout<<"AC:";
        mostrarregistrador(AC);
    }

    //OPERACAO LOAD MQ,X
    //TRANSFERE O CONTEUDO X DA MEMORIA PARA O MQ
    if(compare(IR,"001001")){
        //COPIA O ENDERECO ARMAZENADO EM IR EM MAR
        copia_os_10b_do_registador1_para_registrador2(IR,MAR);
        //COPIA VALOR ARMAZENADO NO ENDERECO DE MAR NO MBR
        copia_word_para_registrador(principal.mem[decimal_registrador_16B(MAR)],MBR);
        //COPIA VALOR ARMAZENADO NO MBR NO MQ
        copia_registrador1_para_registrador2(MBR,MQ);
        cout<<"LOAD MQ, "<<decimal_registrador_16B(MAR)<<endl;
        cout<<"MBR:";
        mostrarregistrador(MBR);
        cout<<"MQ:";
        mostrarregistrador(MQ);
    }

    //OPERACAO STOR X
    //TRANSFERE O CONTEUDO AC  PARA O LOCAL X DA MEMORIA
    if(compare(IR,"100001")){
        //COPIA O ENDERECO ARMAZENADO EM IR EM MAR
        copia_os_10b_do_registador1_para_registrador2(IR,MAR);
        //COPIA VALOR ARMAZENADO NO ENDERECO DE MAR NO MBR
        copia_registrador1_para_registrador2(AC,MBR);
        copia_registrador_para_word(principal.mem[decimal_registrador_16B(MAR)],MBR);
        cout<<"STOR "<<decimal_registrador_16B(MAR)<<endl;
    }

    //OPERACAO LOAD X
    if(compare(IR,"000001")){
        //COPIA O ENDERECO ARMAZENADO EM IR EM MAR
        copia_os_10b_do_registador1_para_registrador2(IR,MAR);
        //COPIA VALOR ARMAZENADO NO ENDERECO DE MAR NO MBR
        copia_word_para_registrador(principal.mem[decimal_registrador_16B(MAR)],MBR);
        copia_registrador1_para_registrador2(MBR,AC);
        cout<<"LOAD "<<decimal_registrador_16B(MAR)<<endl;
    }

    //OPERACAO ADD X
    if(compare(IR,"000101")){
        //COPIA O ENDERECO ARMAZENADO EM IR EM MAR
        copia_os_10b_do_registador1_para_registrador2(IR,MAR);
        //COPIA VALOR ARMAZENADO NO ENDERECO DE MAR NO MBR
        copia_word_para_registrador(principal.mem[decimal_registrador_16B(MAR)],MBR);
        //SOMA O VALOR ARMAZENADO NO MBR(X) COM AC E GUARDA EM AC
        soma(AC,MBR);
        cout<<"ADD "<< decimal_registrador_16B(MAR)<<endl;
    }

    //OPERACAO SUB X
    if(compare(IR,"000110")){
        //COPIA O ENDERECO ARMAZENADO EM IR EM MAR
        copia_os_10b_do_registador1_para_registrador2(IR,MAR);
        //COPIA VALOR ARMAZENADO NO ENDERECO DE MAR NO MBR
        copia_word_para_registrador(principal.mem[decimal_registrador_16B(MAR)],MBR);
        //SUBTRAI O VALOR ARMAZENADO NO MBR(X) COM AC E GUARDA EM AC
        subtracao(AC,MBR);
        cout<<"SUB "<< decimal_registrador_16B(MAR)<<endl;
    }

    //OPERACAO MUL X
    if(compare(IR,"001011")){
        //COPIA O ENDERECO ARMAZENADO EM IR EM MAR
        copia_os_10b_do_registador1_para_registrador2(IR,MAR);
        //COPIA VALOR ARMAZENADO NO ENDERECO DE MAR NO MBR
        copia_word_para_registrador(principal.mem[decimal_registrador_16B(MAR)],MBR);
        //MULTIPLICA O CONTEUDO DA MEMORIA NO ENDERECO X POR MQ E ARMAZENA EM AC
        multiplicacao(AC,MBR,MQ);
        cout<<"MUL "<< decimal_registrador_16B(MAR)<<endl;
    }

    //OPERACAO DIV  X
    if(compare(IR,"001100")){
        //COPIA O ENDERECO ARMAZENADO EM IR EM MAR
        copia_os_10b_do_registador1_para_registrador2(IR,MAR);
        //COPIA VALOR ARMAZENADO NO ENDERECO DE MAR NO MBR
        copia_word_para_registrador(principal.mem[decimal_registrador_16B(MAR)],MBR);
        //SUBTRAI O VALOR ARMAZENADO NO MBR(X) COM AC E GUARDA EM AC
        divisao(AC,MBR,MQ);
        cout<<"DIV "<< decimal_registrador_16B(MAR)<<endl;
    }

    //INCREMENTA P
    binario(PC,decimal_registrador_16B(PC)+1);
}

void busca(memoria &principal,registrador &PC,registrador &IR,registrador &MAR,registrador &MBR,registrador &AC,registrador &MQ){
    binario(PC,0);
    while(decimal_registrador_16B(PC) != 511){
        //COPIA O VALOR APONTADO POR PC NO MAR
        copia_registrador1_para_registrador2(PC,MAR);
        //COPIA O VALOR ARMAZENDA NO ENDERECO CONTIDO NO MAR NO MBR
        copia_word_para_registrador(principal.mem[decimal_registrador_16B(MAR)],MBR);
        //COPIA O VALOR ARMAZENADO EM MBR PARA IR
        copia_registrador1_para_registrador2(MBR,IR);
        execucao(principal,PC,IR,MAR,MBR,AC,MQ);
        //INCREMENTA PC
    }
}

void space(int n){
    for(int i=0;i<n;i++)
        cout <<endl;
}

void zerarmemoria(memoria &principal){
    for(int i=0;i<1023;i++){
        binario(principal.mem[i],0);
    }
}

void instrucoes(memoria &principal,int inicio){
    gravar(principal.mem[inicio]   ,"0010011001011010");
    gravar(principal.mem[inicio+1] ,"0010111001011001");
    gravar(principal.mem[inicio+2] ,"0010100000000000");
    gravar(principal.mem[inicio+3] ,"0001011001011000");
    gravar(principal.mem[inicio+4] ,"1000011001011111");
    gravar(principal.mem[inicio+5] ,"0010011001011100");
    gravar(principal.mem[inicio+6] ,"0010111001011101");
    gravar(principal.mem[inicio+7] ,"0010100000000000");
    gravar(principal.mem[inicio+8] ,"1000011001100000");
    gravar(principal.mem[inicio+9] ,"0000011001011011");
    gravar(principal.mem[inicio+10],"0001101001100000");
    gravar(principal.mem[inicio+11],"1000011001100001");
    gravar(principal.mem[inicio+12],"0000011001011011");
    gravar(principal.mem[inicio+13],"0001101001100000");
    gravar(principal.mem[inicio+14],"1000011001100001");
    gravar(principal.mem[inicio+15],"0000011001011111");
    gravar(principal.mem[inicio+16],"0011001001100001");
    gravar(principal.mem[inicio+17],"1000011001011111");
    gravar(principal.mem[inicio+18],"0010100000000000");
    gravar(principal.mem[inicio+19],"1000011001011110");

    //DADOS

    binario(principal.mem[600],37); //A
    binario(principal.mem[601],80); //B
    binario(principal.mem[602],23); //C
    binario(principal.mem[603],16); //D
    binario(principal.mem[604],63); //E
    binario(principal.mem[605],11); //F
    binario(principal.mem[606],0);  //X
}

void menu(memoria &principal){
    int op,op2,end,valor,cont=0,x;
    cout << "SIMULADOR DE UM PROCESSADOR"<<endl;
    do{
        cout <<"MENU:"<<endl;
        cout <<"1-ADICIONAR UM VALOR A UM ENDERECO DE MEMORIA( 512 A 1023)."<<endl;
        cout <<"2-ADICIONAR UMA INSTRUCAO NA MEMORIA."<<endl;
        cout <<"-1 - SAIR."<<endl;
        cout <<"DIGITE A SUA OPCAO:";
        cin >> op;
        while(op<-1 || op>2){
            cout <<"OPCAO INVALIDA!!! DIGITE NOVAMENTE:";
            cin >> op;
        }
        if(op == 1){
            cout <<"DIGITE O ENDERECO DE MEMORIA A SER ATRIBUIDO O VALOR:";
            cin >>end;
            while(end <512 || end >1023){
                cout <<"ENDERECO NAO PERMITIDO!!! DIGITE UM VALOR ENTRE 512 E 1023:";
                cin >> end;
            }
            cout <<"DIGITE O VALOR A SER ARMAZENADO NO ENDERECO DE MEMORIA "<<end<<" :";
            cin >>valor;
            while(valor <-32677 || valor >32677){
                cout <<"VALOR NAO PERMITIDO!!! DIGITE UM VALOR ENTRE -32677 E 32677:";
                cin >> valor;
            }
            binario(principal.mem[end],valor);
        }else if(op==2){
            space(5);
            cout << "ESCOLHA O TIPO DE COMANDO QUE VC QUER ENSERIR:"<<endl;
            cout <<"1-LOAD MQ"<<endl;
            cout <<"2-LOAD MQ, X"<<endl;
            cout <<"3-STOR X"<<endl;
            cout <<"4-LOAD X"<<endl;
            cout <<"5-ADD X"<<endl;
            cout <<"6-SUB X"<<endl;
            cout <<"7-MUL X"<<endl;
            cout <<"8-DIV X"<<endl;
            cout <<"-2 - SAIR."<<endl;
            cout <<"DIGITE A SUA OPCAO:";
            cin >> op2;
            while(op2<-2 || op2>8){
                cout <<"OPCAO INVALIDA!!! DIGITE NOVAMENTE:";
                cin >> op2;
            }
            if(op2 == 1){
                gravar(principal.mem[cont],"0010100000000000");
                cont++;
            }
            if(op2 == 2){
                cout <<"DIGITE O ENDERECO DE MEMORIA A TRNASFERIR(X):";
                cin >>end;
                while(end <512 || end >1023){
                    cout <<"ENDERECO NAO PERMITIDO!!! DIGITE UM VALOR ENTRE 512 E 1023:";
                    cin >> end;
                }
                gravar(principal.mem[cont],"001001"+bin(end));
                cont++;
            }
            if(op2 == 3){
                cout <<"DIGITE O ENDERECO DE MEMORIA (X) PARA RECEBER AC:";
                cin >>end;
                while(end <512 || end >1023){
                    cout <<"ENDERECO NAO PERMITIDO!!! DIGITE UM VALOR ENTRE 512 E 1023:";
                    cin >> end;
                }
                gravar(principal.mem[cont],"100001"+bin(end));
                cont++;
            }
            if(op2 == 4){
                cout <<"DIGITE O ENDERECO DE MEMORIA A TRNASFERIR(X) PARA AC:";
                cin >>end;
                while(end <512 || end >1023){
                    cout <<"ENDERECO NAO PERMITIDO!!! DIGITE UM VALOR ENTRE 512 E 1023:";
                    cin >> end;
                }
                gravar(principal.mem[cont],"000001"+bin(end));
                cont++;
            }
            if(op2 == 5){
                cout <<"DIGITE O ENDERECO DE MEMORIA (X) PARA SOMAR COM AC:";
                cin >>end;
                while(end <512 || end >1023){
                    cout <<"ENDERECO NAO PERMITIDO!!! DIGITE UM VALOR ENTRE 512 E 1023:";
                    cin >> end;
                }
                gravar(principal.mem[cont],"000101"+bin(end));
                cont++;
            }
            if(op2 == 6){
                cout <<"DIGITE O ENDERECO DE MEMORIA (X) PARA SUBTRAIR COM AC:";
                cin >>end;
                while(end <512 || end >1023){
                    cout <<"ENDERECO NAO PERMITIDO!!! DIGITE UM VALOR ENTRE 512 E 1023:";
                    cin >> end;
                }
                gravar(principal.mem[cont],"000110"+bin(end));
                cont++;
            }
            if(op2 == 7){
                cout <<"DIGITE O ENDERECO DE MEMORIA (X) PARA MULTIPLICAR COM MQ:";
                cin >>end;
                while(end <512 || end >1023){
                    cout <<"ENDERECO NAO PERMITIDO!!! DIGITE UM VALOR ENTRE 512 E 1023:";
                    cin >> end;
                }
                gravar(principal.mem[cont],"001011"+bin(end));
                cont++;
            }
            if(op2 == 8){
                cout <<"DIGITE O ENDERECO DE MEMORIA (X) A SER DIVIDIDO COM AC:";
                cin >>end;
                while(end <512 || end >1023){
                    cout <<"ENDERECO NAO PERMITIDO!!! DIGITE UM VALOR ENTRE 512 E 1023:";
                    cin >> end;
                }
                gravar(principal.mem[cont],"001100"+bin(end));
                cont++;
            }
        }
    }while(op!=-1);
}

#endif // FUNCOES_H
